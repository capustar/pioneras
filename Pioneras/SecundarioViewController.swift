//
//  SecundarioViewController.swift
//  Pioneras
//
//  Created by mastermoviles on 14/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import UIKit

class SecundarioViewController: UIViewController
{
    @IBOutlet weak var texto: UITextView!
    
    var nomFich = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let path = Bundle.main.path(forResource: nomFich, ofType: "txt")
        {
            do
            {
                var texto_pionera = ""
                
                texto_pionera = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                
                self.texto.text = texto_pionera
            }
            catch
            {
                var mensaje_error = ""
                
                mensaje_error = "[EL TEXTO NO ESTA DISPONIBLE]"
                
                self.texto.text = mensaje_error
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
