//
//  ViewController.swift
//  Pioneras
//
//  Created by mastermoviles on 31/10/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var button_ada: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func retornoDeSecundaria(segue : UIStoryboardSegue)
    {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        //obtenemos el controller destino y forzamos la conversión al tipo adecuado
        let controller = segue.destination as! SecundarioViewController
        //fijamosla propiedad "nomFich" al identificador del segue
        controller.nomFich = segue.identifier!
    }
    

}

